compile:
	./generator.py > public/index.html

watch:
	while true; do inotifywait -e modify generator.py database/*yaml static/*; ./generator.py > public/index.html; done

clean:
	rm public/index.html
